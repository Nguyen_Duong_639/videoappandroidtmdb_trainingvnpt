package com.example.videoappandroidtmdb_trainingvnpt.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.videoappandroidtmdb_trainingvnpt.MovieDetail;
import com.example.videoappandroidtmdb_trainingvnpt.R;
import com.example.videoappandroidtmdb_trainingvnpt.model.CategoryItem;

import java.util.List;

public class ItemRecyclerView extends RecyclerView.Adapter<ItemRecyclerView.ItemViewHolder> {

    Context context;
    List<CategoryItem> categoryItemList;
    public Context getContext() {
        return context;
    }

    public ItemRecyclerView(Context context, List<CategoryItem> categoryItemList) {
        this.context = context;
        this.categoryItemList = categoryItemList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.category_recyclerview_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Glide.with(context).load(categoryItemList.get(position).getImageUrl()).into(holder.itemImage);
        holder.textView.setText(categoryItemList.get(position).getMovieName());

        holder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /// điều kiện giá trị của biến tvshow để lọc tv và movie
                Intent intent = null;
                if (categoryItemList.get(position).getTvshow() != null) {
                    intent = new Intent(context, MovieDetail.class);
                    intent.putExtra("tvshow", categoryItemList.get(position).getTvshow());
                } else {
                    intent = new Intent(context, MovieDetail.class);
                }

                /// điều kiện giá trị của biến tvshow để lọc tv và movie

                intent.putExtra("MovienameID", categoryItemList.get(position).getId());
                intent.putExtra("Moviename", categoryItemList.get(position).getMovieName());
                intent.putExtra("MovieImageUrl", categoryItemList.get(position).getImageUrl());
                intent.putExtra("Backdrop", categoryItemList.get(position).getBackdrop());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryItemList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView textView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            textView = itemView.findViewById(R.id.movienametv);
        }
    }
}
