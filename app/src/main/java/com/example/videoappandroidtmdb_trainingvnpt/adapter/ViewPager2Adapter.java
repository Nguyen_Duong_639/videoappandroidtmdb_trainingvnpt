package com.example.videoappandroidtmdb_trainingvnpt.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.videoappandroidtmdb_trainingvnpt.MovieDetail;
import com.example.videoappandroidtmdb_trainingvnpt.R;
import com.example.videoappandroidtmdb_trainingvnpt.model.CategoryItem;

import java.util.List;

public class ViewPager2Adapter extends RecyclerView.Adapter<ViewPager2Adapter.ViewHolder> {
    Context context;
    private List<CategoryItem> categoryItems;

    public ViewPager2Adapter(Context context, List<CategoryItem> categoryItems) {
        this.context = context;
        this.categoryItems = categoryItems;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.banner_movie_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull  ViewPager2Adapter.ViewHolder holder, int position) {
        holder.nameMovie.setText(categoryItems.get(position).getMovieName());
        holder.content.setText(categoryItems.get(position).getSummary());
        Glide.with(context).load(categoryItems.get(position).getBackdrop()).into(holder.imageMovie);


        holder.imageMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /// điều kiện giá trị của biến tvshow để lọc tv và movie
                Intent intent = null;
                if (categoryItems.get(position).getTvshow() != null) {
                    intent = new Intent(context, MovieDetail.class);
                    intent.putExtra("tvshow", categoryItems.get(position).getTvshow());
                } else {
                    intent = new Intent(context, MovieDetail.class);
                }

                /// điều kiện giá trị của biến tvshow để lọc tv và movie
                intent.putExtra("MovienameID", categoryItems.get(position).getId());
                intent.putExtra("Moviename", categoryItems.get(position).getMovieName());
                intent.putExtra("MovieImageUrl", categoryItems.get(position).getImageUrl());
                intent.putExtra("Backdrop", categoryItems.get(position).getBackdrop());
                context.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return categoryItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageMovie;
        TextView nameMovie,content;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageMovie=itemView.findViewById(R.id.banner_image);
            nameMovie=itemView.findViewById(R.id.banner_name);
            content=itemView.findViewById(R.id.banner_content);
        }
    }

}
