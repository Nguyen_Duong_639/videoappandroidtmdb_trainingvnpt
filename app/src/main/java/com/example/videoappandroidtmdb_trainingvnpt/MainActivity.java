package com.example.videoappandroidtmdb_trainingvnpt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.videoappandroidtmdb_trainingvnpt.fragment.BlankFragment;
import com.example.videoappandroidtmdb_trainingvnpt.fragment.FavoriteFragment;
import com.example.videoappandroidtmdb_trainingvnpt.fragment.HomeFragment;
import com.example.videoappandroidtmdb_trainingvnpt.fragment.PopularFragment;
import com.example.videoappandroidtmdb_trainingvnpt.fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private Bundle bundle = new Bundle();
    private  BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottomnavigationview);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, new HomeFragment()).commit();


    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.homeitem:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.popularitem:
                            selectedFragment = new PopularFragment();
                            break;
                        case R.id.favoriteitem:
                            selectedFragment = new FavoriteFragment();
                            break;
                        case R.id.profileitem:
                            selectedFragment = new ProfileFragment();
                            break;
                        default:

                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,selectedFragment).commit();
                    selectedFragment.setArguments(bundle);//// chuyền dữ liệu vào fragment bằng bundle
                    return true;
                }

            };
}