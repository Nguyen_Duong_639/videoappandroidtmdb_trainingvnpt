package com.example.videoappandroidtmdb_trainingvnpt;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.videoappandroidtmdb_trainingvnpt.json.JsonVNPT;
import com.example.videoappandroidtmdb_trainingvnpt.model.Cast;
import com.example.videoappandroidtmdb_trainingvnpt.model.CategoryItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class MovieDetail extends AppCompatActivity {
    private TextView moviename;
    private ImageView movieImage, likemovie, share;
    private String keymovie;
//    private Button playButton;
//    private RecyclerView mainRecyclerview;
//    private RecyclerView relatedRecyclerview;
//    private List<CategoryItem> categoryItemList;
//    private List<Cast> castList;
    private CategoryItem movieDetailinfomation;
    private TextView movieTime, movieVote, movieCategory, movieSummary, movieDate, movieTitle;
    int tvshow;
    int mID;
    private JsonVNPT json = new JsonVNPT();
    private String k;
    private String namemovie;
    private int anhlike = 1;
    private ImageView ratingim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_movie_detail);
        moviename = findViewById(R.id.movie_name);
        movieImage = findViewById(R.id.movie_image_detail);
//        playButton = findViewById(R.id.play_button);
        likemovie = findViewById(R.id.likemovie);
        ratingim = findViewById(R.id.ratingim);

//////
        movieTime = findViewById(R.id.timemovie);
        movieVote = findViewById(R.id.movievote);
        movieCategory = findViewById(R.id.movieCategory);
        movieSummary = findViewById(R.id.movieSummary);
        movieDate = findViewById(R.id.movieDate);
        movieTitle = findViewById(R.id.movieTitle);

        // get data from Intent
        mID = getIntent().getExtras().getInt("MovienameID");
        String mname = getIntent().getStringExtra("Moviename");
        String mImage = getIntent().getStringExtra("MovieImageUrl");
        String mFileUrl = getIntent().getStringExtra("Backdrop");
        tvshow = getIntent().getExtras().getInt("tvshow");
        namemovie = mname;
        this.getmoviekey(mID);

        Glide.with(this).load(mFileUrl).into(movieImage);
        moviename.setText(mname);

        this.jsonmovieDetail();

        setClicklikemovie();

        this.setPlayButton();
        setClickRatingitem();

    }

    private void setClickRatingitem() {
        ratingim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog(v);
            }
        });
    }

    private void ShowDialog(View view) {
        Button ratingbt, deleterating;
        RatingBar ratingBar;
        Dialog dialog = new Dialog(view.getContext());
        dialog.setContentView(R.layout.dialog_choose_rating);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        deleterating = dialog.findViewById(R.id.deleterating);
        ratingbt = dialog.findViewById(R.id.ratingbt);
        ratingBar = dialog.findViewById(R.id.ratingitembar);
        ratingbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(MovieDetail.this);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                String url = "https://api.themoviedb.org/3/movie/" + mID + "/rating?api_key=9ed4a1f097a3e78ed51133843d2156ea&session_id=59489d368f113315f024f7a5e390730070cecf6c";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: " + error.getMessage());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("value", rating + "");
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });
        deleterating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.themoviedb.org/3/movie/" + mID + "/rating?api_key=9ed4a1f097a3e78ed51133843d2156ea&session_id=59489d368f113315f024f7a5e390730070cecf6c";
                StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "onResponse: " + response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "onErrorResponse: " + error.getMessage());
                    }
                }) {

                };
                requestQueue.add(stringRequest);

                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);// tìm hiểu thêm
        dialog.show();
    }

    private void setClicklikemovie() {
        likemovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestQueue requestQueue = Volley.newRequestQueue(MovieDetail.this);
                if (anhlike == 1) {
                    likemovie.setImageResource(R.drawable.love);
                    anhlike = 2;
                } else {
                    likemovie.setImageResource(R.drawable.unlove);
                    anhlike = 1;
                }
                JSONObject object2 = new JSONObject();
                try {

                    object2.put("media_type", "movie");
                    object2.put("media_id", mID);
                    if (anhlike == 1) {
                        object2.put("favorite", false);
                    } else {
                        object2.put("favorite", true);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url = "https://api.themoviedb.org/3/account/10144263/favorite?api_key=9ed4a1f097a3e78ed51133843d2156ea&session_id=59489d368f113315f024f7a5e390730070cecf6c";
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object2,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    @Override       //Send Header
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();
                        params.put("api_call_header", "header_value");

                        return params;
                    }
                };
                requestQueue.add(request);
            }
        });
    }

    private void setPlayButton() {
        movieImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MovieDetail.this, PlayMovieActivity.class);
                i.putExtra("url", keymovie);
                startActivity(i);
            }
        });
    }

    private void getmoviekey(int kh) {
        Toast.makeText(MovieDetail.this, kh + "", Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(MovieDetail.this);
        String category = "movie";
        if (tvshow == 1) {
            category = "tv";
        }
        keymovie = "rong";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://api.themoviedb.org/3/" + category + "/" + kh + "/videos?api_key=9ed4a1f097a3e78ed51133843d2156ea&language=en-US",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            keymovie = json.JSONmovieTrailer(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MovieDetail.this, "Lỗi", Toast.LENGTH_SHORT).show();
                Log.d("AAA", "Lỗi\n" + error.toString());
            }
        });
        requestQueue.add(stringRequest);

    }

    private void jsonmovieDetail() {
        RequestQueue requestQueue = Volley.newRequestQueue(MovieDetail.this);
        movieDetailinfomation = new CategoryItem();
        String category = "movie";
        if (tvshow == 1) {
            category = "tv";
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, " https://api.themoviedb.org/3/"
                + category + "/"
                + mID +
                "?api_key=9ed4a1f097a3e78ed51133843d2156ea&language=vi-VN",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {

                            movieDetailinfomation = json.JSONmovieDetail(response);
                            movieTime.setText(movieDetailinfomation.getTime());
                            movieCategory.setText(movieDetailinfomation.getGenres());
                            movieSummary.setText(movieDetailinfomation.getSummary());
                            movieDate.setText(movieDetailinfomation.getYearOfRelease());
                            movieVote.setText(movieDetailinfomation.getVote());
                            movieTitle.setText(movieDetailinfomation.getTitle());

                            /// lấy dữ liệu đẩy lên firebase
//                            firebaseHistoryWatched.addUserObject(new HistoryMovieWatched(mID, namemovie, movieDetailinfomation.getImageUrl(), movieDetailinfomation.getSummary()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MovieDetail.this, "Lỗi", Toast.LENGTH_SHORT).show();
                Log.d("AAA", "Lỗi\n" + error.toString());
            }
        });
        requestQueue.add(stringRequest);

    }

}